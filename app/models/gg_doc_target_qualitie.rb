class GgDocTargetQualitie < ActiveRecord::Base
  include Redmine::SafeAttributes


  safe_attributes 'code','name','parent_id','quality_type','checklists','template','material_type'
  attr_accessible :code, :name, :parent_id, :quality_type,:checklists,:template,:material_type
end