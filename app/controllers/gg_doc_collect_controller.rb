class GgDocCollectController < ApplicationController
  before_filter :find_project_by_project_id
  include ApplicationHelper

  helper :attachments
  include AttachmentsHelper

  def common
    if @project.module_enabled?(:gg_doc_collect)
      gant_tracker = Setting.plugin_gg_doc_collect['gantt_tracker']
      relation_tracker = Setting.plugin_gg_doc_collect['target_quality_tracker']
      ## 横道图任务
      project = Project.find(params[:project_id])
      gant_project_id = project[:parent_id]
      gantt_issues = Issue
                         .joins("left join issue_statuses on issues.status_id = issue_statuses.id ")
                         .where("project_id = ? and tracker_id = ?",gant_project_id,gant_tracker)
                         .select("issues.*,issue_statuses.name")
                         .order("issues.subject")
      @result_issues = []
      ## 检查列表总数
      @all_checklistnum = 0
      ##已选检查列表数
      @all_docnum = 0
      if gantt_issues.length > 0
        days = Setting.plugin_gg_doc_collect['doc_collect_post_due_date'] ? Setting.plugin_gg_doc_collect['doc_collect_post_due_date'].to_i : 0
        gantt_issues.each do |i|
          date = i[:due_date] ? i[:due_date].to_time + days.day : ""
          result_issue = {}
          num = 0
          c_num = 0
          a_num = 0
          ##关联的文控任务
          relation_issues = Issue
                                .joins("left join issue_statuses on issues.status_id = issue_statuses.id ")
                                .where("project_id = ? and tracker_id = ?  and (issues.id in (select issue_to_id  from issue_relations where issue_from_id = ? ) or issues.id in (select issue_from_id  from issue_relations where issue_to_id = ? ))", params[:project_id],relation_tracker, i.id, i.id)
                                .select("issues.*,issue_statuses.name")
          if relation_issues.length > 0
            ## 查询关联的文控任务的检查列表
            relation_issues.each do |p|
              cheack_num = 0
              collect_num = 0
              cheack_item = EasyChecklistItem.joins("left join (select id, entity_type,entity_id from easy_checklists where entity_type = 'Issue' and entity_id = "+p[:id].to_s+")ct on easy_checklist_items.easy_checklist_id = ct.id")
                                            .select("easy_checklist_items.*")
                                            .where("entity_id is not null")
              collect_item = EasyChecklistItem.joins("left join (select id,entity_type,entity_id from easy_checklists where entity_type = 'Issue' and entity_id = "+p[:id].to_s+")ct on easy_checklist_items.easy_checklist_id = ct.id")
                                              .where("easy_checklist_items.done != false and entity_id is not null")
                                              .select("easy_checklist_items.*")
              attachment_num = Attachment.where("container_id = ? and container_type = 'Issue'",p[:id]).count
              if cheack_item.length > 0
                cheack_num = cheack_item.count
              end
              if collect_item.length > 0
                collect_num = collect_item.count
              end
              num = num + cheack_num
              c_num = c_num + collect_num
              a_num = a_num + attachment_num
            end
          end
          result_issue = {
            :id =>i[:id],
            :value=>i[:id],
            :label =>i[:subject],
            :project_id=>i[:project_id],
            :tracker_id =>i[:tracker_id],
            :status_name =>i[:name],
            :name =>i[:subject],
            :doc_due_date => date.to_time.strftime('%Y-%m-%d'),
            :parent_id =>i[:parent_id],
            :doc_issues =>relation_issues.as_json,
            :doc_check_num =>num,
            :doc_collect_num =>c_num,
            :doc_attachment =>a_num,
          }
          @result_issues.push(result_issue)
          @all_checklistnum +=  num
          @all_docnum += c_num
        end
      end
    end
    respond_to do |format|
      format.api
    end
  end

  def index
    @user_edit = User.current.allowed_to?(:gg_doc_collect_edit, @project)
    respond_to do |format|
      format.html
    end
  end

  def index_s
    @user_edit = User.current.allowed_to?(:gg_doc_collect_edit, @project)
    respond_to do |format|
      format.html{ render layout: 'single_layout' }
    end
  end

  def match_list
    gant_tracker = Setting.plugin_gg_doc_collect['gantt_tracker']
    quality_tracker = Setting.plugin_gg_doc_collect['target_quality_tracker']
    gant_project_id = @project[:parent_id]
    gantt_issues = Issue.where("project_id = ? and tracker_id = ?", gant_project_id, gant_tracker).order("subject")
    @gantt_issues = []
    gantt_issues.each do |gant|
       gantt = {}
       gantt[:id] = gant[:id]
       gantt[:project_id] = gant[:project_id]
       gantt[:tracker_id] = gant[:tracker_id]
       gantt[:parent_id] = gant[:parent_id]
       gantt[:subject] = gant[:subject]
       gantt[:quality] = false
       relation_issue = Issue
                            .where("project_id = ? and tracker_id in (?) and issues.id in (select issue_to_id  from issue_relations where issue_from_id = ? ) or issues.id in (select issue_from_id  from issue_relations where issue_to_id = ? )",@project[:id],quality_tracker,gant[:id],gant[:id])
       if relation_issue.length > 0
         gantt[:quality] = true
       end
       @gantt_issues.push(gantt)
    end
    @target_quality = GgDocTargetQualitie.all

    respond_to do |format|
      format.html
      format.api
    end
  end

  def quality_issues_match
     gantt_issues_ids= Array.new(params[:gantt_issues_ids]){ |e| e.to_i  }
     quality_ids = Array.new(params[:quality_ids]){  |q|  q.to_i  }
     gant_project_id = @project[:parent_id]
     gant_tracker = Setting.plugin_gg_doc_collect['gantt_tracker']
     quality_tracker = Setting.plugin_gg_doc_collect['target_quality_tracker']
     gantt_issues = Issue.where("project_id = ?  and id in (?) and tracker_id = ? ", gant_project_id, gantt_issues_ids, gant_tracker)
     quality_datas = GgDocTargetQualitie.where("id in (?)", quality_ids)
     create_quality_issues(quality_datas,gantt_issues,quality_tracker)
     respond_to do |format|
       format.json { render :json => {result: "success"} }
     end
   end

  def create_quality_issues(quality_datas,gantt_issues,quality_tracker)
    pm_default_author = Setting.plugin_gg_doc_collect['doc_pm_default_author'].to_i
    days = Setting.plugin_gg_doc_collect['doc_collect_post_due_date'].blank? ? 0 : Setting.plugin_gg_doc_collect['doc_collect_post_due_date'].to_i
    if !Setting.plugin_gg_doc_collect['custom_field_gcfl_target_quality_issue'].blank? &&
      !Setting.plugin_gg_doc_collect['custom_field_gf_target_quality_issue'].blank? &&
      !Setting.plugin_gg_doc_collect['custom_field_gfcode_target_quality_issue'].blank? &&
      !Setting.plugin_gg_doc_collect['custom_field_fbgc_target_quality_issue'].blank? &&
      !Setting.plugin_gg_doc_collect['custom_field_zfb_target_quality_issue'].blank? &&
      !Setting.plugin_gg_doc_collect['custom_field_fxdy_target_quality_issue'].blank?
      ActiveRecord::Base.transaction do
        quality_datas.each do |qdata|
        ###查询的资料库
          ##分享/单元
          fxdy = GgDocTargetQualitie.find(qdata[:id])
          ##子分部
          zfb = GgDocTargetQualitie.find_by(:id =>fxdy[:parent_id],:quality_type => '4')
          ##分部工程
          if !zfb.blank?
            fbgc = GgDocTargetQualitie.find(zfb[:parent_id])
          else
            fbgc = GgDocTargetQualitie.find(fxdy[:parent_id])
          end
          ##规范
          gf = GgDocTargetQualitie.find(fbgc[:parent_id])
          ##工程分类
          gcfl = GgDocTargetQualitie.find(gf[:parent_id])
          gantt_issues.each do |gi|
          date = gi[:due_date].blank? ? "" : (gi[:due_date].to_time + days.day)
          status = IssueStatus.find(gi[:status_id])
          ##新建质量评定任务
          new_quality_issue = Issue.new(
              :author_id => pm_default_author,
              :created_on => Time.now,
              :project_id => @project[:id],
              :tracker_id => quality_tracker.to_i,
              :start_date => gi[:start_date],
              :subject => gi[:subject] + "【" + qdata[:name] + "】",
              :due_date => date,
          )
        ##自动赋值
          cf_fxdy = CustomField.find(Setting.plugin_gg_doc_collect['custom_field_fxdy_target_quality_issue'].to_i)
          if !cf_fxdy.blank? && !fxdy.blank?
            new_quality_issue.custom_field_value(cf_fxdy) << fxdy[:name]
          end
          cf_zfb = CustomField.find(Setting.plugin_gg_doc_collect['custom_field_zfb_target_quality_issue'].to_i)
          if !cf_zfb.blank? && !zfb.blank?
            new_quality_issue.custom_field_value(cf_zfb) << zfb[:name]
          end
          cf_fbgc = CustomField.find(Setting.plugin_gg_doc_collect['custom_field_fbgc_target_quality_issue'].to_i)
          if !cf_fbgc.blank? && !fbgc.blank?
            new_quality_issue.custom_field_value(cf_fbgc) << fbgc[:name]
          end
          cf_gf = CustomField.find(Setting.plugin_gg_doc_collect['custom_field_gf_target_quality_issue'].to_i)
          if !cf_gf.blank? && !gf.blank?
            new_quality_issue.custom_field_value(cf_gf) << gf[:name]
          end
          cf_gfcode = CustomField.find(Setting.plugin_gg_doc_collect['custom_field_gfcode_target_quality_issue'].to_i)
          if !cf_gfcode.blank? && !gf.blank?
            new_quality_issue.custom_field_value(cf_gfcode) << gf[:code]
          end
          cf_gcfl = CustomField.find(Setting.plugin_gg_doc_collect['custom_field_gcfl_target_quality_issue'].to_i)
          if !cf_gcfl.blank? && !gcfl.blank?
            new_quality_issue.custom_field_value(cf_gcfl) << gcfl[:name]
          end
        ##新建质量评定和横道图任务关联
          if new_quality_issue.save
            relation = IssueRelation.new
            relation[:issue_from_id] =  new_quality_issue[:id]
            relation[:issue_to_id] = gi[:id]
            relation[:relation_type] = 'relates'
            relation.save
          end
        ##生成文件编号
          auto_code = GgHandlingforeasy::AutoCode::AutoCode.new
          lsh = auto_code.generate_file_code(new_quality_issue).last
          ##流水号
          cf_lsh = CustomField.find(Setting.plugin_gg_doc_collect['custom_field_doc_serial_number'].to_i)
          if !cf_lsh.blank? && !Setting.plugin_gg_doc_collect['custom_field_doc_serial_number'].blank?
            new_quality_issue.custom_field_value(cf_lsh) << lsh
          end
          ##审批流
          call_hook(:controller_issues_new_after_save, {  :params => {}, :issue => new_quality_issue})
          if !status[:name].include?("计划")
            ###横道图任务状态为进行中时,赋值完成日期和跟踪者
            rule_ids = Setting.plugin_gg_doc_collect['target_quality_rule'].split(",")
            if !rule_ids.blank?
              rule = Array.new(rule_ids){ |rl| rl.to_i  }
              user_ids = Member
                             .joins("LEFT JOIN member_roles on members.id = member_roles.member_id")
                             .where("member_roles.role_id in (?) and members.project_id = ?",rule,new_quality_issue[:project_id])
                             .select("members.user_id")
              if user_ids.length > 0
                user_ids.each do |ui|
                  old_watcher = Watcher.find_by(:watchable_id => new_quality_issue[:id], :watchable_type => 'Issue', :user_id => ui[:user_id])
                  if old_watcher.blank?
                    watcher = Watcher.new
                    watcher[:watchable_type] = 'Issue'
                    watcher[:watchable_id] = new_quality_issue[:id]
                    watcher[:user_id] = ui[:user_id]
                    watcher.save!
                  end
                end

              end
            end
            GgIssueProcessController.special_approval_pass(pm_default_author,new_quality_issue[:id])
          end
        ## 质量评定任务中新增检查列表
        if !qdata[:checklists].blank?
          new_checklist = EasyChecklist.new(
              :name => '材料质量检查列表',
              :author_id => User.current,
              :entity_id =>new_quality_issue[:id],
              :entity_type =>'Issue',
              :created_at => Time.now)
          if new_checklist.save!
            items = qdata[:checklists].split("\n")
            items.each do |t|
              new_checklist_item = EasyChecklistItem.new(
                  :subject =>t,
                  :easy_checklist_id => new_checklist[:id],
                  :author_id => User.current,
                  :created_at => Time.now,
                  :changed_by_id => User.current
              )
              new_checklist_item.save!
            end
          end
        end
        end
      end
      end
    end
  end

  def get_doc_word_template_url
    id = params[:id]
    attachment = Attachment.find(id)
    if not attachment.blank?
      send_file attachment.diskfile
    else
      puts ("error")
    end
  end

  def quality_query
    quality_name = params[:quality_name]
    quality_data = GgDocTargetQualitie.find_by("quality_type = '5' and name = ?",quality_name)
    container_id = quality_data[:id]
    @attachments = Attachment.where("container_type = 'GgDocTargetQualitie' and  container_id = ?",container_id)
                      .last
    respond_to do |format|
      format.api
    end
  end

  def get_doc_word_template_relation_issue
    issue_id = params[:issue_id]
    gant_tracker = Setting.plugin_gg_doc_collect['gantt_tracker']
    @relation_issue = Issue
                            .where(" tracker_id in (?) and issues.id in
                                    (select issue_to_id  from issue_relations where issue_from_id = ? )
                                    or issues.id in (select issue_from_id  from issue_relations where issue_to_id = ? )",
                                   gant_tracker, issue_id, issue_id)
                            .first
    respond_to do |format|
      format.api
    end
  end

end