class GgProjectDocCollectController < ApplicationController

   def index
     respond_to do |format|
       format.html
     end
   end

   def index_s
     respond_to do |format|
       format.html{ render layout: 'single_layout' }
     end
   end

   def common
    cf_project_type = CustomField.find_by(:type=>'ProjectCustomField',:name=>'组织类型')
    if !cf_project_type.blank?
      cf_project_depart = CustomField.find_by(:type=>'ProjectCustomField',:name=>'部门负责人')
      @projects = Project.where("id in (select customized_id from custom_values where customized_type = 'Project' and custom_field_id = ? and value = '文控管理')",cf_project_type[:id])
                         .select("id")
      if @projects.length > 0
        @project_results = []
        status = IssueStatus.find_by(:name => "进行中")
        tracker_ids = Setting.plugin_gg_doc_collect['target_quality_tracker'].split(",")
        tracker_ids.each do |ti|
          ti.to_i
        end
        today = "'" + Time.new.strftime("%Y-%m-%d") + "'"
        select_results = Issue
                               .joins("left join projects on issues.project_id = projects.id")
                               .joins("left join easy_checklists on issues.id = easy_checklists.entity_id and easy_checklists.entity_type = 'Issue'
                                        left join easy_checklist_items
                                        on easy_checklists.id = easy_checklist_items.easy_checklist_id")
                               .where("issues.status_id = ? and issues.tracker_id in (?) and projects.id in (?)",status[:id].to_i,tracker_ids,@projects)
                               .select("issues.project_id ,projects.name as  name ,count(easy_checklist_items.id)as checklist,sum(case when easy_checklist_items.done and issues.due_date <= "+today+" then 0 else 1 end )as uncollect")
                               .group("issues.project_id,projects.name")
        @projects.each do |p|
          project = Project.find(p[:id])
          depart_header=project.custom_field_value(cf_project_depart)
          header = ""
          if  !depart_header.blank?
           de_head = User.find(depart_header.to_i)
           header = de_head.name
          end
          if project.module_enabled?(:gg_doc_collect)
            rel = {}
            checklist = 0
            uncollect = 0
            select_results.each do |sr|
              if sr[:project_id] == p[:id]
                checklist = sr[:checklist]
                uncollect = sr[:uncollect]
              end
            end
            rel[:project_id] = p[:id]
            rel[:project_name] = project[:name]
            rel[:checklist] = checklist
            rel[:uncollect] = uncollect
            rel[:depart_header] = header
            @project_results.push(rel)
          end
        end
      end
    end
    respond_to do |format|
      format.api
    end
  end


private
end