class GgDocTargetQualityController < ApplicationController

  def index
    @datas =  GgDocTargetQualitie.all
    @materials = GgMaterial.all
    respond_to do |format|
      format.html
      format.api
    end
  end

  def parent_query
    quality_parent_id = params[:quality_parent_id]
    @querydatas = GgDocTargetQualitie.where("quality_type <= ? ", quality_parent_id)
                                     .order("code")
    respond_to do |format|
      format.api
    end
  end

  def new
    file_list = params[:fileList]
    quality_new = params[:new_quality]
    new_quality = GgDocTargetQualitie.new
    new_quality[:quality_type] = quality_new[:quality_type]
    new_quality[:created_at] = DateTime.now
    new_quality[:name] = quality_new[:name]
    new_quality[:parent_id] = quality_new[:parent_id]
    new_quality[:code] = quality_new[:code]
    new_quality[:checklists] = quality_new[:checklists]
    new_quality[:template] = quality_new[:template]
    new_quality[:material_type] = quality_new[:material_type]
    new_quality.save!
    if !file_list.blank?
      file_list.each do |file|
        attach = Attachment.find(file)
        attach.update_attributes(:container_id => new_quality[:id],:container_type => 'GgDocTargetQualitie')
        attach.save!
      end
    end
    respond_to do |format|
      format.json { render :json => {result: "success"} }
    end
  end

  def delete
    gg_target_quality = GgDocTargetQualitie.find(params[:id])
    deleteStatus = false
    if gg_target_quality.destroy!
      deleteStatus = true
    end
    respond_to do |format|
      format.json { render :json => deleteStatus }
    end
  end

  def edit
    @edit_quality = GgDocTargetQualitie.find(params[:id])
    @file_list = Attachment.where("container_id = ? and container_type = 'GgDocTargetQualitie' ",params[:id]).order('created_on').last
    if !@edit_quality[:parent_id].blank?
      @parent_datas = GgDocTargetQualitie.where("quality_type < ? ", @edit_quality[:quality_type])
    else
      @parent_datas = ""
    end
    respond_to do |format|
      format.api
    end
  end

  def update
    updateStatus = false
    quality_edit =  params[:quality_edit]
    quality_id = params[:id]
    quality = GgDocTargetQualitie.find(quality_id)
    if !quality_edit[:template].blank?
      quality.update_attributes(quality_edit)
    else
      quality.update_attributes(
          :quality_type =>quality_edit[:quality_type],
          :name =>quality_edit[:name],
          :parent_id =>quality_edit[:parent_id],
           :code =>quality_edit[:code],
          :checklists =>quality_edit[:checklists],
         :material_type => quality_edit[:material_type]
      )
    end
    if quality.save!
      updateStatus = true
    end
    file_list = params[:fileList]
    if !file_list.blank?
      file_list.each do |file|
        attach = Attachment.find(file)
        attach.update_attributes(:container_id => quality_id,:container_type => 'GgDocTargetQualitie')
        attach.save!
      end
    end
    respond_to do |format|
      format.json{
        render :json => updateStatus
      }
    end
  end

  def get_temlData

  end

end