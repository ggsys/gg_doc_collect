class CreateGgDocTargetQualities < ActiveRecord::Migration
  def change
    create_table :gg_doc_target_qualities do |t|
      t.string  :code
      t.string  :name
      t.integer  :parent_id
      t.string  :quality_type
      t.string  :template
      t.string  :checklists
      t.string  :material_type
      t.timestamps  :created_on
    end
  end
end
