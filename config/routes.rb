
RedmineApp::Application.routes.draw do

  match "/projects/:project_id/gg_doc_collect/index" => "gg_doc_collect#index", via: [:get, :post]
  match "/projects/:project_id/gg_doc_collect/index_s" => "gg_doc_collect#index_s", via: [:get, :post]
  match "/projects/:project_id/gg_doc_collect/common" => "gg_doc_collect#common", via: [:get, :post]


  match "/gg_doc_collect/project_menu/index" => "gg_project_doc_collect#index", via: [:get, :post]
  match "/gg_doc_collect/project_menu/index_s" => "gg_project_doc_collect#index_s", via: [:get, :post]
  match "/gg_doc_collect/project_menu/common" => "gg_project_doc_collect#common", via: [:get, :post]

  match "/gg_doc_file_auto/admin_menu/index" => "gg_doc_file_auto#index", via: [:get, :post]

  match "/gg_doc_target_quality/admin_menu/index" => "gg_doc_target_quality#index", via: [:get, :post]
  match "/gg_doc_target_quality/admin_menu/parent_query" => "gg_doc_target_quality#parent_query", via: [:get, :post]
  match "/gg_doc_target_quality/admin_menu/new" => "gg_doc_target_quality#new", via: [:get, :post]
  match "/gg_doc_target_quality/:id/delete" => "gg_doc_target_quality#delete", via: [:get, :post]
  match "/gg_doc_target_quality/:id/edit" => "gg_doc_target_quality#edit", via: [:get, :post]
  match "/gg_doc_target_quality/:id/update" => "gg_doc_target_quality#update", via: [:get, :post]

  match "/projects/:project_id/gg_doc_collect/match_list" => "gg_doc_collect#match_list", via: [:get, :post]
  match "/projects/:project_id/gg_doc_collect/quality_issues_match" => "gg_doc_collect#quality_issues_match", via: [:get, :post]

  match "/projects/:project_id/:id/doc_template/word_template_url" => "gg_doc_collect#get_doc_word_template_url", via: [:get, :post]

  match "/projects/:project_id/doc_collect/quality_query" => "gg_doc_collect#quality_query", via: [:get, :post]
  match "/projects/:project_id/doc_collect/:issue_id/get_doc_word_template_relation_issue" => "gg_doc_collect#get_doc_word_template_relation_issue", via: [:get, :post]
end

