require_dependency File.dirname(__FILE__) + '/lib/gg_doc_collect.rb'

Redmine::Plugin.register :gg_doc_collect do
  name '文控管理'
  author 'lixinan'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'

  settings :default => {
      'gantt_tracker'=>'',
      'doc_collect_post_due_date' => '0',
      'target_quality_tracker' => '',
      'doc_pm_default_author'=>'',
      'target_quality_rule'=>'',
      'target_quality_issue_status_epw'=>'',
      'custom_field_gcfl_target_quality_issue' => '',
      'custom_field_gf_target_quality_issue' => '',
      'custom_field_gfcode_target_quality_issue' => '',
      'custom_field_fbgc_target_quality_issue' => '',
      'custom_field_zfb_target_quality_issue' => '',
      'custom_field_fxdy_target_quality_issue' => '',
      'custom_field_doc_serial_number' => '',
  }, :partial => 'issues/settings'


  project_module :gg_doc_collect do
    permission :gg_doc_collect_view,
               {
                   :gg_doc_collect => [:index,:index_s],
               }, :require => :member
    permission :gg_doc_collect_edit,
               {:gg_doc_collect => [:quality_issues_match, :match_list],
               }, :require => :member

  end

  menu :project_menu, :gg_doc_collect, {:controller => 'gg_doc_collect', :action => 'index' },
       :caption => :label_gg_doc_collect, :before => :setting, :param => :project_id


  menu :admin_menu, :gg_doc_collect, {:controller => 'gg_doc_target_quality', :action => 'index' },
       :caption => :label_gg_doc_quality_assessment


end
