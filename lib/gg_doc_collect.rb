Rails.configuration.to_prepare do
  require 'hooks/update_document_data_after_edit_save_hooks'
  require 'hooks/hdy_doctarget_quality_export_word_hooks'
  require 'hooks/update_document_data_before_edit_save_hooks'
end
