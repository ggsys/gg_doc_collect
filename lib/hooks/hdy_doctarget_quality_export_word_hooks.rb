# 导出到word
module GgDocCollect
  class DoctargetQualityExportWordHooks < Redmine::Hook::ViewListener
    render_on :view_issue_sidebar_top,
              :partial => 'issues/doctarget_quality_export_word'
  end
end
