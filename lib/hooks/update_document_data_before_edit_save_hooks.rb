module GgDocCollect
  class Hooks < Redmine::Hook::ViewListener
      def controller_issues_edit_before_save(context={ })
        issue = context[:issue]
        gant_tracker = Setting.plugin_gg_doc_collect['gantt_tracker']
        tracker_ids = Setting.plugin_gg_doc_collect['target_quality_tracker'].split(",")
        if issue.tracker_id.to_s == gant_tracker
          old_issue = Issue.find(issue[:id])
          status = IssueStatus.find(issue[:status_id])
          if old_issue[:status_id] != issue[:status_id] && status[:name].include?("进行中")
            days = Setting.plugin_gg_doc_collect['doc_collect_post_due_date'].blank? ?  0 : Setting.plugin_gg_doc_collect['doc_collect_post_due_date'].to_i
            date = issue[:due_date].blank? ?  "" : issue[:due_date].to_time + days.day
            relation_issues = Issue.where("tracker_id in (?) and id in (select issue_to_id  from issue_relations where issue_from_id = ? ) or id in (select issue_from_id  from issue_relations where issue_to_id = ? )",tracker_ids,issue.id,issue.id)
            rule_ids = Setting.plugin_gg_doc_collect['target_quality_rule'].split(",")
            rule = Array.new(rule_ids){ |e| e.to_i  }
            relation_issues.each do |i|
              project = Project.find(i[:project_id])
              if project.module_enabled?(:gg_doc_collect)
                if status[:name] != '已完成'
                  i.update_attributes(:start_date => issue[:start_date],:due_date=> date,:status_id => issue[:status_id])
                  i.save
                  if status[:name].include?("进行中")
                    if !rule_ids.blank?
                      user_ids = Member
                                     .joins("LEFT JOIN member_roles on members.id = member_roles.member_id")
                                     .where("member_roles.role_id in (?) and members.project_id = ?", rule, project[:id])
                                     .select("members.user_id")
                      if user_ids.length > 0
                        user_ids.each do |ui|
                          old_watcher = Watcher.find_by(:watchable_id => i.id, :watchable_type => 'Issue', :user_id => ui[:user_id])
                          if old_watcher.blank?
                            watcher = Watcher.new
                            watcher[:watchable_type] = 'Issue'
                            watcher[:watchable_id] = i.id
                            watcher[:user_id] = ui[:user_id]
                            watcher.save!
                          end
                        end
                      end
                    end
                    GgIssueProcessController.special_approval_pass(i[:author_id], i[:id])
                  end
                end
              end
            end
          end
        end
      end
  end
end
